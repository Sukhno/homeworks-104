1. Які існують типи даних у Javascript?

2. У чому різниця між == і ===?

3. Що таке оператор?

Відповіді:

1. Які існують типи даних у Javascript?

У JavaScript є 8 основних типів даних:

number для будь-яких чисел: цілих чисел з плаваючою точкою, цілі численні значення обмежені діапазоном ±2^53.

bigint для цілих чисел довільної довжини.

string для рядків. Рядок може містити один або більше символів, немає окремого символьного типу.

'Boolean' для 'true/false'.

null для невідомих значень – окремий тип, що має одне значення null.

undefined для неприсвоєних значень – окремий тип, що має одне значення undefined.

object для складніших структур даних.

symbol для унікальних ідентифікаторів.

2. У чому різниця між == і ===?

Оператор == перевіряє, чи дорівнюють значення після перетворення їх типу даних до спільного типу.

Оператор суворої рівності === перевіряє рівність без перетворення типів.

3. Що таке оператор?

Оператор - це символ або набір символів, що використовуються для виконання певної операції зі значеннями. Оператори використовуються в виразах для виконання обчислень, порівнянь і присвоєння значень змінним, арифметичні, порівняння, логічні, присвоєння, тернарні, та інші.
