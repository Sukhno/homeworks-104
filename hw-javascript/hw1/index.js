// Task 1
let name = "Oleksandra";
let admin = name;
console.log(admin);

// Task 2
const days = 5;
const seconds = days * 86400;
console.log(seconds);
// 24*60*60 = 86 400;

// Task 3
const age = +prompt("How old are you?");
console.log(age);