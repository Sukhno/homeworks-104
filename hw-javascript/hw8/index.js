// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphs = document.getElementsByTagName('p');
for (let i = 0; i < paragraphs.length; i++) {
  paragraphs[i].style.backgroundColor = '#ff0000';
}

// 2. Знайти елемент із id="optionsList". Вивести у консоль.
// Знайти батьківський елемент та вивести в консоль.Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const optionsList = document.getElementById('optionsList');
console.log(optionsList);

const parentElement = optionsList.parentNode;
console.log(parentElement);

const childNodes = optionsList.childNodes;
for (let i = 0; i < childNodes.length; i++) {
  console.log(childNodes[i].nodeName + ': ' + childNodes[i].nodeType);
}

// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
// const testParagraph = document.querySelector('.testParagraph'); - у html немає такого класу, але є id

const testParagraph = document.getElementById('testParagraph');
testParagraph.innerText = 'This is a paragraph';


// 4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const mainHeader = document.querySelector('.main-header');
const mainHeaderElements = mainHeader.querySelectorAll('*');
for (let i = 0; i < mainHeaderElements.length; i++) {
    mainHeaderElements[i].classList.add('nav-item');
    console.log(mainHeaderElements[i]);
}

// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitles = document.querySelectorAll('.section-title');
for (let i = 0; i < sectionTitles.length; i++) {
  sectionTitles[i].classList.remove('section-title');
}
