function togglePassword(inputId, icon) {
    const input = document.getElementById(inputId);
    if (input.type === "password") {
        input.type = "text";
        icon.classList.remove("fa-eye");
        icon.classList.add("fa-eye-slash");
    } else {
        input.type = "password";
        icon.classList.remove("fa-eye-slash");
        icon.classList.add("fa-eye");
    }
}

function comparePasswords(event) {
    event.preventDefault();
    const password1 = document.getElementById("password1").value;
    const password2 = document.getElementById("password2").value;
    const errorMessage = document.getElementById("error-message");
    if (password1 === password2) {
        alert("You are welcome");
        errorMessage.innerText = "";
    } else {
        errorMessage.innerText = "Потрібно ввести однакові значення";
    }
}
