function doList(array, parent = document.body) {
  const ul = document.createElement('ul');
  
  array.forEach((item) => {
    const li = document.createElement('li');
    const text = document.createTextNode(item);
    li.appendChild(text);
    ul.appendChild(li);
  });
  
  parent.appendChild(ul);
}

doList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"] );
