1. Опишіть, як можна створити новий HTML тег на сторінці.
2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
3. Як можна видалити елемент зі сторінки?

Відповіді:

1. Опишіть, як можна створити новий HTML тег на сторінці.

   document.createElement(tag) – створює елемент з заданим тегом.
   Наприклад, створення тегу div:
   let div = document.createElement('div');

2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

   Функція insertAdjacentHTML - це метод DOM-елемента, який дозволяє додавати HTML-код до сторінки в певному місці відносно вказаного елемента.

   targetElement.insertAdjacentHTML(position, text).
   Перший параметр функції insertAdjacentHTML - це рядок, який визначає, куди потрібно вставити HTML-код. Цей параметр може приймати чотири значення:
   "beforebegin": до відкриваючого тега.
   "afterbegin": після відкриваючого тега.
   "beforeend": перед закриваючим тегом .
   "afterend": після закриваючого тега.

3. Як можна видалити елемент зі сторінки?

   Метод remove() - це найпростіший спосіб видалення елемента зі сторінки.

   const element = document.getElementById('elementId');
   element.remove();

   Метод parentNode.removeChild() - метод видаляє вказаний елемент, використовуючи його батьківський елемент.

   const element = document.getElementById('elementId');
   const parent = element.parentNode;
   parent.removeChild(element);

   Метод replaceWith() - метод може бути використаний для заміни елемента на інший елемент або для видалення елемента. Щоб видалити елемент потрібно використати цей метод, передаючи в якості параметра null.

   const element = document.getElementById('elementId');
   element.replaceWith(null);
