const tabs = document.querySelectorAll('.tabs-title');
        const tabsContent = document.querySelectorAll('.tabs-content li');

        tabs.forEach((tab, index) => {
            tab.addEventListener('click', () => {
                tabs.forEach(tab => tab.classList.remove('active'));
                tabsContent.forEach(content => content.classList.remove('active'));
                tab.classList.add('active');
                tabsContent[index].classList.add('active');
            });
        });