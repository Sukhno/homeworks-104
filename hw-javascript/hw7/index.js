function filterBy(arr, dataType) {
  return arr.filter(function(elem) {
    return typeof elem !== dataType;
  });
}

let arr = ['hello', 'world', 23, '23', null];
let newArray = filterBy(arr, 'string');
console.log(newArray);




